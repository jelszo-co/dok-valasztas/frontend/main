import { SET_USER_BANNED } from './types';

export const setUserBanned = (banned: boolean, bannedTill?: string) => (dispatch: any) => {
  if (banned && !bannedTill) throw Error;
  let payload: { banned: boolean; bannedTill?: number } = { banned };
  localStorage.setItem('banned', String(banned));
  if (banned) {
    const bannedTillParsed = Date.now() + parseInt(bannedTill ? bannedTill : '0') * 1000;
    payload.bannedTill = bannedTillParsed;
    bannedTill && localStorage.setItem('bannedTill', String(bannedTillParsed));
  }
  return dispatch({ type: SET_USER_BANNED, payload });
};

export const getUserDataFromLocalstorage = () => (dispatch: any) => {
  const banned = localStorage.getItem('banned');
  const bannedParsed: boolean | null = banned ? JSON.parse(banned) : null;
  let bannedTillParsed: number | null = null;
  if (banned) {
    const bannedTill = localStorage.getItem('bannedTill');
    if (!bannedTill) throw Error;
    bannedTillParsed = JSON.parse(bannedTill);
  }
  if (bannedParsed && bannedTillParsed) {
    if (Date.now() > bannedTillParsed) {
      localStorage.removeItem('banned');
      localStorage.removeItem('bannedTill');
      return dispatch({ type: SET_USER_BANNED, payload: { banned: false, bannedTill: undefined } });
    }
    console.log('User is banned until ' + new Date(bannedTillParsed));
    dispatch({ type: SET_USER_BANNED, payload: { banned: true, bannedTill: bannedTillParsed } });
  }
};
