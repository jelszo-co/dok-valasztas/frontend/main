import { PageStatus, Timing } from 'types';
import axios from 'axios';
import { setPopupWithObject } from './popup';
import { errPopupServer } from '../templates/errPopup';
import { GET_TIMINGS } from './types';

export const getTimings = () => async (dispatch: any) => {
  try {
    const { data }: { data: Timing[] } = await axios.get(
      `${process.env.REACT_APP_SRV_ADDR}/status/timing`,
    );
    return dispatch({ type: GET_TIMINGS, payload: data });
  } catch (err) {
    console.error(err);
    setPopupWithObject(errPopupServer);
  }
};
