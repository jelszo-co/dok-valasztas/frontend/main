import { Candidate } from 'types';
import { DEL_CANDIDATE_MODAL, SET_CANDIDATE_MODAL } from './types';

export const setCandidateModal = (c: Candidate) => async (dispatch: any) => {
  dispatch({ type: SET_CANDIDATE_MODAL, payload: c });
};

export const delCandidateModal = () => async (dispatch: any) =>
  dispatch({ type: DEL_CANDIDATE_MODAL });
