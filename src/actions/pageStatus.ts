import axios from 'axios';
import { errPopupServer } from 'templates/errPopup';
import { GET_PAGE_STATUS, SET_POPUP } from './types';
import { API_status_current } from '../types/apiTypes';
import { PageStatus } from '../types';
import { getWinnerCandidates } from './winnerCandidates';

export const getPageStatus = () => async (dispatch: any) => {
  try {
    const { data }: { data: API_status_current } = await axios.get(
      `${process.env.REACT_APP_SRV_ADDR}/status/current`,
    );
    if (data.current === PageStatus.Result) dispatch(getWinnerCandidates());
    return dispatch({ type: GET_PAGE_STATUS, payload: data.current });
  } catch (err) {
    dispatch({ type: SET_POPUP, payload: errPopupServer });
    console.error(err);
  }
};
