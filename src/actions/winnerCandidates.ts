import axios from 'axios';
import { errPopupServer } from 'templates/errPopup';
import { GET_VOTE_COUNT, GET_WINNER_CANDIDATES, SET_POPUP } from './types';
import { API_candidate_results__instance, API_candidate_winner__instance } from '../types/apiTypes';

export const getWinnerCandidates = () => async (dispatch: any) => {
  try {
    const { data: winner }: { data: API_candidate_winner__instance[] } = await axios.get(
      `${process.env.REACT_APP_SRV_ADDR}/candidate/winner`,
    );
    dispatch({ type: GET_WINNER_CANDIDATES, payload: winner });
    const { data: count }: { data: API_candidate_results__instance[] } = await axios.get(
      `${process.env.REACT_APP_SRV_ADDR}/candidate/results`,
    );
    dispatch({ type: GET_VOTE_COUNT, payload: count });
  } catch (err) {
    dispatch({ type: SET_POPUP, payload: errPopupServer });
    console.error(err);
  }
};
