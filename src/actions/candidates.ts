import axios from 'axios';
import { errPopupServer } from 'templates/errPopup';
import { Candidate } from 'types';
import { GET_CANDIDATES, SET_POPUP } from './types';

export const getCandidates = () => async (dispatch: any) => {
  try {
    const { data }: { data: Candidate[] } = await axios.get(
      `${process.env.REACT_APP_SRV_ADDR}/candidate`,
    );
    dispatch({
      type: GET_CANDIDATES,
      payload: data,
    });
  } catch (err) {
    dispatch({ type: SET_POPUP, payload: errPopupServer });
    console.error(err);
  }
};
