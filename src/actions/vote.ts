import { Candidate } from 'types';
import { UPDATE_VOTE_DATA, UPDATE_VOTE_PROGRESS, UPDATE_VOTE_STAGE } from './types';

interface PartialVoteData {
  om?: string;
  chosenCandidate?: Candidate;
}

export const updateVoteData = (data: PartialVoteData) => (dispatch: any) =>
  dispatch({ type: UPDATE_VOTE_DATA, payload: data });

export const setStage = (stage: number) => (dispatch: any) =>
  dispatch({ type: UPDATE_VOTE_STAGE, payload: stage });

export const setProgress = (progress: number) => (dispatch: any) =>
  dispatch({ type: UPDATE_VOTE_PROGRESS, payload: progress });
