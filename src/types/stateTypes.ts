export interface VoteData {
  om: string;
  chosenCandidate?: number;
  stage: number;
  progress: number;
}

export interface UserData {
  banned: boolean;
  bannedTill?: number;
}
