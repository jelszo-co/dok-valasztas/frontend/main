import { PageStatus } from '../types';

export interface API_status_current {
  current: PageStatus;
}

export interface API_candidate_winner__instance {
  candidate: number;
}

export interface API_candidate_results__instance {
  id: number;
  result: number;
}
