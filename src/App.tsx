import { ComponentType, useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';

import Landing from './pages/Landing';
import Vote from 'pages/Vote';
import MarkdownRenderer from 'pages/MarkdownRenderer';
import e404 from 'pages/e404';

import PopupWrapper from './pages/components/PopupWrapper';
import Preloader from './pages/components/Preloader';

import './scss/App.scss';

import { getPageStatus } from './actions/pageStatus';
import { getCandidates } from 'actions/candidates';
import { getTimings } from './actions/timings';
import { getUserDataFromLocalstorage } from './actions/user';

import privacyMarkdown from 'assets/Adatkezelesi_Tajekoztato_DOK.md';
import faqMarkdown from 'assets/FAQ.md';

const himnusz = `
\tIsten, áldd meg a magyart
Jó kedvvel, bőséggel,
Nyújts feléje védő kart,
Ha küzd ellenséggel;
Bal sors akit régen tép,
Hozz rá víg esztendőt,
Megbünhödte már e nép
A multat s jövendőt!

\tŐseinket felhozád
Kárpát szent bércére,
Általad nyert szép hazát
Bendegúznak vére.
S merre zúgnak habjai
Tiszának, Dunának,
Árpád hős magzatjai
Felvirágozának.

\tÉrtünk Kunság mezein
Ért kalászt lengettél,
Tokaj szőlővesszein
Nektárt csepegtettél.
Zászlónk gyakran plántálád
Vad török sáncára,
S nyögte Mátyás bús hadát
Bécsnek büszke vára.

\tHajh, de bűneink miatt
Gyúlt harag kebledben,
S elsújtád villámidat
Dörgő fellegedben,
Most rabló mongol nyilát
Zúgattad felettünk,
Majd töröktől rabigát
Vállainkra vettünk.

\tHányszor zengett ajkain
Ozman vad népének
Vert hadunk csonthalmain
Győzedelmi ének!
Hányszor támadt tenfiad
Szép hazám kebledre,
S lettél magzatod miatt
Magzatod hamvvedre!

\tBújt az üldözött s felé
Kard nyúl barlangjában,
Szerte nézett s nem lelé
Honját a hazában,
Bércre hág és völgybe száll,
Bú s kétség mellette,
Vérözön lábainál,
S lángtenger fölette.

\tVár állott, most kőhalom,
Kedv s öröm röpkedtek,
Halálhörgés, siralom
Zajlik már helyettek.
S ah, szabadság nem virúl
A holtnak véréből,
Kínzó rabság könnye hull
Árvánk hő szeméből!

\tSzánd meg Isten a magyart
Kit vészek hányának,
Nyújts feléje védő kart
Tengerén kínjának.
Bal sors akit régen tép,
Hozz rá víg esztendőt,
Megbünhödte már e nép
A multat s jövendőt!
`;

const App: ComponentType = () => {
  useEffect(() => {
    store.dispatch<any>(getPageStatus());
    store.dispatch<any>(getCandidates());
    store.dispatch<any>(getTimings());
    store.dispatch<any>(getUserDataFromLocalstorage());

    if (Math.trunc(Math.random() * 1000) === 122) {
      let i = 0;
      setInterval(() => {
        if (i === himnusz.length - 16) {
          document.title = 'DÖK Szavazás';
        } else {
          document.title = himnusz.substr(i, 16);
          i++;
        }
      }, 100);
    }
  }, []);
  return (
    <Provider store={store}>
      <PopupWrapper />
      <Preloader />
      <svg className='base-gradient-holder'>
        <defs>
          <linearGradient id='base-gradient' x1='0%' y1='0%' x2='0%' y2='100%'>
            <stop offset='0' stopColor='#ef233c' />
            <stop offset='1' stopColor='#ff8208' />
          </linearGradient>
        </defs>
      </svg>
      <Router>
        <Switch>
          <Route exact path='/' component={Landing} />
          <Route exact path='/vote' component={Vote} />
          <Route
            exact
            path='/privacy-policy'
            component={(props: any) => <MarkdownRenderer markdown={privacyMarkdown} {...props} />}
          />
          <Route
            exact
            path='/faq'
            component={(props: any) => (
              <MarkdownRenderer markdown={faqMarkdown} className='faq' {...props} />
            )}
          />
          <Route component={e404} />
        </Switch>
      </Router>
    </Provider>
  );
};

export default App;
