import React, { ChangeEvent, useEffect, useRef } from 'react';
import { Link, Redirect, RouteComponentProps, withRouter } from 'react-router-dom';
import classNames from 'classnames';
import axios, { AxiosError } from 'axios';
import { connect } from 'react-redux';
import { PageStatus, PopupType, State } from 'types';
import { VoteData } from 'types/stateTypes';

import {
  checkmarkCircleOutline,
  checkmarkOutline,
  chevronBackCircle,
  informationCircleOutline,
} from 'ionicons/icons';
import IonIcon from 'pages/components/IonIcon';

import { Candidate, CandidateCardOverview, CandidateCardSelect } from '@jelszo-co/dok-components';
import { errPopupRatelimit, errPopupServer } from 'templates/errPopup';

import { setPopup, setPopupWithObject } from 'actions/popup';
import { setProgress, setStage, updateVoteData } from 'actions/vote';
import { setUserBanned } from 'actions/user';

import 'scss/Vote.scss';
import isMobile from '../utils/isMobile';

interface VoteProps extends RouteComponentProps {
  setPopup: Function;
  setPopupWithObject: Function;
  updateVoteData: Function;
  candidates: Candidate[];
  vote: VoteData;
  pageStatus: PageStatus;
  setStage: Function;
  setProgress: Function;
  setUserBanned: Function;
}

const Vote = ({
  setPopup,
  setPopupWithObject,
  candidates,
  history,
  vote,
  updateVoteData,
  setStage,
  setProgress,
  setUserBanned,
  pageStatus,
}: VoteProps) => {
  // ****************
  // SVG ANIMATION
  // ****************

  const svgContainer = useRef<SVGSVGElement>(null);

  const rnd = (max: number) => Math.trunc(Math.random() * max);

  /**
   * Gives back a random number around a starting point
   * @param start Starting number
   * @param scatt Scatter (how much the random value can differ from the starting number)
   * @returns The random number
   * @example rndScatter(100, 10) => number between 90 - 110
   */
  const rndScatter = (start: number, scatt: number): number =>
    Math.trunc(Math.random() * scatt + (start - scatt));

  const rndNotIn = (max: number, notIn: Array<number> = [], notInScatt: number = 0) => {
    let num: number;
    const notInScattered: Array<Array<number>> = notIn.map(n => [n - notInScatt, n + notInScatt]);

    console.log('rndNotIn', max, notIn, notInScatt, notInScattered);

    do {
      num = rnd(max);
    } while (notInScattered.some(el => num >= el[0] && num <= el[1]));

    return num;
  };

  interface svgFragment {
    type: string;
    controlX?: number;
    controlY?: number;
    x: number;
    y: number;
  }

  let allPathArray: Array<svgFragment> = [];

  const genRandomPath = (points: number): string => {
    const vw = window.innerWidth * 1.3;
    const vh = window.innerHeight * 1.3;
    const minQ = 100;

    let x = rnd(vw);
    let y = rnd(vh);
    let pathArray: Array<svgFragment> = [];

    pathArray.push({ type: 'M', x, y });
    allPathArray.push({ type: 'M', x, y });

    for (let i = 0; i < points - 1; i++) {
      x = rndNotIn(
        vw,
        allPathArray.map(q => q.x),
        vw / 10,
      );
      y = rndNotIn(
        vh,
        allPathArray.map(q => q.y),
        vh / 10,
      );
      const controlX = rndScatter(x, rnd(400) + minQ);
      const controlY = rndScatter(y, rnd(300) + minQ);
      pathArray.push({ type: 'Q', controlX, controlY, x, y });
      allPathArray.push({ type: 'Q', controlX, controlY, x, y });
      console.log('loop passed');
    }

    pathArray.push({
      type: '',
      x: rndNotIn(
        vw,
        allPathArray.map(q => q.x),
        vw / 10,
      ),
      y: rndNotIn(
        vh,
        allPathArray.map(q => q.y),
        vh / 10,
      ),
    });

    // M x y
    // Q controlX controlY endX endY
    return pathArray
      .map(
        ({ type, controlX, controlY, x, y }) =>
          ` ${type} ${controlX && controlY ? controlX + ' ' + controlY + ', ' : ''}${x} ${y}`,
      )
      .join('');
  };

  const svgns = 'http://www.w3.org/2000/svg';

  const drawPaths = (instances: number, points: number): Array<Element> => {
    const pathArray: Array<Element> = [];
    allPathArray = [];
    const defs = document.createElementNS(svgns, 'defs');

    for (let i = 0; i < instances; i++) {
      const grad = document.createElementNS(svgns, 'linearGradient');
      grad.id = `vote-path-grad-${i}`;
      grad.setAttribute('gradientTransform', `rotate(${rnd(360)})`);

      for (let j = 0; j < 2; j++) {
        const stop = document.createElementNS(svgns, 'stop');
        stop.setAttribute('offset', j.toString());
        stop.setAttribute('stop-color', j === 0 ? '#ef233c' : '#ff8208');
        grad.appendChild(stop);
      }

      defs.appendChild(grad);
    }
    pathArray[0] = defs;

    for (let i = 1; i < instances + 1; i++) {
      const path = genRandomPath(points);
      const polyline = document.createElementNS(svgns, 'path');
      polyline.setAttribute('d', path);
      polyline.setAttribute('stroke', `url(#vote-path-grad-${i - 1})`);
      polyline.classList.add('vote-path');
      pathArray[i] = polyline;
    }
    return pathArray;
  };

  const instances = isMobile() ? 1 : 2,
    points = 3;

  const redraw = () => {
    const { current } = svgContainer;
    if (current)
      current.childNodes.forEach(n => {
        const node: any = n;
        node.classList.remove('path-visible');
      });
    setTimeout(() => {
      if (current)
        while (current.firstChild) {
          const lastChild: any = current.lastChild;
          current.removeChild(lastChild);
        }

      const paths = drawPaths(instances, points);
      paths.forEach(path => current && current.appendChild(path));
      setTimeout(() => {
        current?.childNodes.forEach((node: any) => node.classList.add('path-visible'));
      }, 300);
    }, 1000);
  };

  useEffect(() => {
    const paths = drawPaths(instances, points);
    paths.forEach(path => {
      svgContainer.current && svgContainer.current.appendChild(path).classList.add('path-visible');
    });
  }, []);

  // *************
  // STAGES
  // *************

  const updateStage = (newStage: number, updateProgress = false) => {
    if (updateProgress && newStage > progress) setProgress(newStage);
    if (newStage > progress + (updateProgress ? 1 : 0)) return;
    if (newStage === stage) return;
    const els = document.querySelectorAll('.vote-stage');
    els.forEach(e => e.classList.remove('vote-stage-visible'));
    setTimeout(() => {
      redraw();
      setStage(newStage);
      const els = document.querySelectorAll('.vote-stage');
      els.forEach(e => e.classList.add('vote-stage-visible'));
      if (newStage === 3) {
        const el: HTMLElement | null = document.querySelector('#vote-stage-final');
        if (!el) return;
        el.style.display = 'block';
        setTimeout(() => {
          el.classList.add('vote-final-visible');
        }, 500);
      }
    }, 500);
  };

  const { om, chosenCandidate, stage, progress } = vote;

  const handleRequestError = (err: any) => {
    const axErr: AxiosError = err;
    if (axErr.response?.data.error === 'NO_SUCH_VOTER')
      return setPopup(PopupType.Error, 'Nincs ilyen adatokkal rendelkező diák!', 5);

    if (axErr.response?.data.error === 'ALREADY_VOTED') {
      return setPopup(PopupType.Alert, 'Te már szavaztál.', 5);
    }

    if (axErr.response?.status === 429) {
      console.log(err.response);
      setUserBanned(true, axErr.response.headers['retry-after']);
      history.push('/');
      return setPopupWithObject(errPopupRatelimit);
    }

    setPopupWithObject(errPopupServer);
  };

  const validateData = async (): Promise<boolean> => {
    if (om.length < 11) return false;

    try {
      await axios.get(`${process.env.REACT_APP_SRV_ADDR}/vote?om=${om}`);
      updateStage(1, true);
      return true;
    } catch (err) {
      handleRequestError(err);
      return false;
    }
  };

  const handleSubmit = async () => {
    if (chosenCandidate === undefined) return;
    try {
      await axios.post(`${process.env.REACT_APP_SRV_ADDR}/vote`, {
        candidate: chosenCandidate,
        om,
      });
      updateStage(3, true);
    } catch (err) {
      handleRequestError(err);
    }
  };

  // *******************
  // RENDER
  // *******************

  const renderVoteStages = () => {
    switch (stage) {
      case 0:
        return (
          <div className='vote-stage vote-stage-visible'>
            <div className='vote-stage-col-left'>
              <label>OM azonosító</label>
              <div className='vote-input-wrapper'>
                <input
                  id='om'
                  name='om'
                  type='text'
                  placeholder='OM azonosító'
                  value={om}
                  onKeyDown={e => {
                    if (e.key === 'Enter') return validateData();
                    if (e.key !== 'Backspace') return;
                    if (om.length === 1) return updateVoteData({ om: '' });
                  }}
                  onChange={(e: ChangeEvent<HTMLInputElement>) =>
                    /(7[0-9]*)$/.test(e.target.value) && updateVoteData({ om: e.target.value })
                  }
                  maxLength={11}
                />
              </div>

              <div className='om-hint'>
                <IonIcon icon={informationCircleOutline} />
                <p>A diákigazolvány alján megtalálható 11 jegyű szám</p>
              </div>
            </div>
            <button
              type='button'
              className={classNames('button-next', { active: om.length === 11 })}
              onClick={() => validateData()}>
              Tovább
            </button>
          </div>
        );
      case 1:
        return (
          <div className='vote-stage vote-stage-selector vote-stage-visible'>
            <div className='candidate-selector'>
              {candidates.map((c, index) => (
                <CandidateCardSelect
                  c={c}
                  key={c.id}
                  selected={c.id === chosenCandidate}
                  click={() => {
                    updateVoteData({ chosenCandidate: candidates[index].id });
                    window.scrollTo({ left: 0, top: 1000000, behavior: 'smooth' });
                  }}
                />
              ))}
            </div>
            <button
              type='button'
              className={classNames('button-next', { active: chosenCandidate !== undefined })}
              onClick={() => {
                chosenCandidate !== undefined && updateStage(2, true);
              }}>
              Tovább
            </button>
          </div>
        );
      case 2:
        return (
          <div className='vote-stage vote-stage-overview vote-stage-visible'>
            <div className='vote-stage-col-left'>
              <h3>OM Azonosító</h3>
              <h4>{om}</h4>
            </div>
            <div className='vote-stage-col-right'>
              <CandidateCardOverview
                // Workaround for not working non-null assertion operator (!)
                c={candidates.find(c => c.id === chosenCandidate) || candidates[0]}>
                Választott
              </CandidateCardOverview>
            </div>
            <button className='button-next active' onClick={() => handleSubmit()}>
              Befejezés!
            </button>
          </div>
        );
      case 3:
        return <></>;
      default:
        return '';
    }
  };

  const stages = ['OM Azonosító', 'Szavazat', 'Véglegesítés'];

  const stageClickHandler = async (index: number) => {
    if (stage === 0) {
      if (await validateData()) updateStage(index);
    } else {
      updateStage(index);
    }
  };

  if (pageStatus !== PageStatus.Vote) return <Redirect to={'/'} />;
  return (
    <div id='vote'>
      <IonIcon
        className='vote-back'
        icon={chevronBackCircle}
        onClick={() => (stage === 0 ? history.goBack() : updateStage(stage - 1))}
      />
      {stage !== 3 && (
        <svg className='vote-paths' xmlns='http://www.w3.org/2000/svg' ref={svgContainer} />
      )}
      <div className='vote-content'>
        <div className='stage-indicator'>
          <span className='stage-indicator-line' />
          {stages.map((indicator, index) => {
            return (
              <div
                key={indicator}
                className={classNames('stage-indicator-object', {
                  'stage-indicator-current': index === stage,
                  'stage-indicator-clickable': index <= progress,
                })}
                onClick={() => stageClickHandler(index)}>
                <span className={classNames({ 'indicator-bg-done': progress >= index })} />
                {stage > index ? (
                  <IonIcon icon={checkmarkOutline} />
                ) : (
                  <p className={classNames({ 'stage-indicator-current-label': index <= progress })}>
                    {index + 1}
                  </p>
                )}
                <h5>{indicator}</h5>
              </div>
            );
          })}
        </div>
        <form
          className='vote-stages'
          onSubmit={e => {
            console.log('submmitted');
            e.preventDefault();
          }}>
          {renderVoteStages()}
        </form>
      </div>
      <div id='vote-stage-final'>
        <div className='content'>
          <IonIcon className='vote-stage-final-checkmark' icon={checkmarkCircleOutline} />
          <h2>Kész vagy!</h2>
        </div>
        <Link to='/'>Főoldal</Link>
      </div>
    </div>
  );
};

const mapStateToProps = (state: State) => ({
  candidates: state.candidates,
  vote: state.vote,
  pageStatus: state.pageStatus,
});

export default connect(mapStateToProps, {
  setPopup,
  setPopupWithObject,
  updateVoteData,
  setStage,
  setProgress,
  setUserBanned,
})(withRouter(Vote));
