import 'scss/e404.scss';
import { GeneralCTA, Colors } from '@jelszo-co/dok-components';
import { RouteComponentProps, withRouter } from 'react-router-dom';
const e404 = ({ history }: RouteComponentProps) => {
  return (
    <div id='e404'>
      <div className='content'>
        <h1>404</h1>
        <div className='err-msg'>
          <p>Az oldal nem létezik!</p>
          <div className='emoji'>😔</div>
        </div>
        <GeneralCTA
          color={Colors.Bright}
          bgColor={Colors.Gradient}
          callback={() => history.push('/')}
          className='cta-button'>
          kezdőlap
        </GeneralCTA>
      </div>
    </div>
  );
};

export default withRouter(e404);
