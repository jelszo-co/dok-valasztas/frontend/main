import { connect } from 'react-redux';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import classNames from 'classnames';
import isEmpty from 'lodash/isEmpty';
import Countdown from 'react-countdown';
import { Candidate, PageStatus, State, Timing } from 'types';
import { UserData } from 'types/stateTypes';
import { API_candidate_results__instance, API_candidate_winner__instance } from '../types/apiTypes';

import Parking from './Parking';
import CandidateModal from './components/CandidateModal';

import {
  CandidateCard,
  Image,
  Colors,
  GeneralCTA,
  SectionHeading,
} from '@jelszo-co/dok-components';

import { ReactComponent as HeaderBG } from 'assets/header/H_backdrop.svg';
import { ReactComponent as HeaderBGObjects } from 'assets/header/H_BG_Objects.svg';
import { ReactComponent as HeaderBlur } from 'assets/header/H_blur.svg';
import { ReactComponent as HeaderFGObjects } from 'assets/header/H_FG_Objects.svg';
import { ReactComponent as VIB } from 'assets/VIB.svg';
import { ReactComponent as VIBmobile } from 'assets/VIBmobile.svg';

import { setCandidateModal } from 'actions/candidateModal';

import 'scss/Landing.scss';
import isMobile from '../utils/isMobile';

interface LandingProps extends RouteComponentProps {
  pageStatus: string;
  candidates: Candidate[];
  setPopup: Function;
  setCandidateModal: Function;
  candidateModal: Candidate;
  timings: Timing[];
  user: UserData;
  getWinnerCandidates: Function;
  winners: API_candidate_winner__instance[];
  results: API_candidate_results__instance[];
}

const Landing = ({
  pageStatus,
  candidates,
  history,
  setCandidateModal,
  candidateModal,
  timings,
  user,
  winners,
  results,
}: LandingProps) => {
  const getTimingForStatus = (status: PageStatus) =>
    timings.filter(timing => timing.status === status);

  const countdownRender = ({
    days,
    hours,
    minutes,
    seconds,
    completed,
    flavor,
  }: {
    days: number;
    hours: number;
    minutes: number;
    seconds: number;
    completed: boolean;
    flavor: string;
  }) => {
    if (completed) return <a onClick={() => window.location.reload()}>Újratöltés</a>;
    return (
      <h3 className={'countdown'}>
        {flavor}{' '}
        <span>
          {days} nap {hours} óra {isMobile() && <br />}
          {minutes} perc {seconds} másodperc
        </span>
      </h3>
    );
  };

  const landingAction = () => {
    switch (pageStatus) {
      case PageStatus.Open:
        if (getTimingForStatus(PageStatus.Vote).length > 0)
          return (
            <Countdown
              date={getTimingForStatus(PageStatus.Vote)[0].start}
              renderer={props => countdownRender({ ...props, flavor: 'Szavazásig hátralévő idő:' })}
            />
          );
        return;
      case PageStatus.Vote:
        if (user.banned)
          return (
            <h3 className={'vote-end-text'}>
              Túl sok hibás próbálkozásod volt. Nézz vissza később!
            </h3>
          );
        return (
          <>
            <GeneralCTA
              color={Colors.Bright}
              bgColor={Colors.Gradient}
              callback={() => history.push('/vote')}
              className='general-cta-vote'>
              Szavazok!
            </GeneralCTA>
            {getTimingForStatus(PageStatus.Vote).length > 0 && (
              <Countdown
                date={getTimingForStatus(PageStatus.Vote)[0].until}
                renderer={props =>
                  countdownRender({ ...props, flavor: 'Még ennyi ideig szavazhatsz:' })
                }
              />
            )}
          </>
        );
      case PageStatus.VoteExpired:
        return (
          <>
            <h3 className={'vote-end-text'}>A szavazás lezárult.</h3>
            {getTimingForStatus(PageStatus.Result).length > 0 && (
              <Countdown
                date={getTimingForStatus(PageStatus.Result)[0].start}
                renderer={props =>
                  countdownRender({ ...props, flavor: 'Eredményhirdetésig hátralévő idő:' })
                }
              />
            )}
          </>
        );
      case PageStatus.Result:
      default:
        return;
    }
  };

  if (pageStatus === PageStatus.Closed) return <Parking />;
  if (candidateModal !== null) return <CandidateModal />;

  return (
    <div id='landing'>
      <header id='header'>
        <HeaderBG id='header-backdrop' />
        <HeaderBGObjects id='header-bg-objects' />
        <HeaderBlur id='header-blur' />
        <HeaderFGObjects id='header-fg-objects' />
        <div className='landing-action-wrapper'>{landingAction()}</div>
      </header>
      <h1>
        Diákönkormányzati elnök választás
        <br />
        2020-2021
      </h1>
      {pageStatus === PageStatus.Result ? (
        <div className='landing-result'>
          <h2 className={'vote-end-text vote-result-text'}>
            A Nyertes{winners.length > 1 && 'ek'}:
          </h2>
          <div className='landing-result-candidates'>
            {candidates
              .filter(c => winners.some(w => w.candidate === c.id))
              .map(c => (
                <div
                  className={classNames('winner-card', {
                    'winner-card-image': !isEmpty(c.picture),
                  })}
                  key={c.id}>
                  {!isEmpty(c.picture) && (
                    <Image base64Image={c.picture ? c.picture : ''} uid={c.id} />
                  )}
                  {c.name.map(name => (
                    <h3 className='candidate-name' key={name}>
                      {name}
                    </h3>
                  ))}
                  <h4 className='winner-card-result'>
                    {results.find(r => r.id === c.id)?.result} szavazat
                  </h4>
                </div>
              ))}
          </div>
          <div className='landing-result-other-candidates'>
            {candidates
              .filter(c => !winners.some(w => w.candidate === c.id))
              .sort(
                (c1, c2) =>
                  // Fuck non-null assertion operator
                  (results.find(r => r.id === c2.id)?.result || 0) -
                  (results.find(r => r.id === c1.id)?.result || 0),
              )
              .map(c => (
                <div className='other-card' key={c.id}>
                  {c.name.map(name => (
                    <h3 className='candidate-name' key={name}>
                      {name}
                    </h3>
                  ))}
                  <h4 className='other-card-result'>
                    {results.find(r => r.id === c.id)?.result} szavazat
                  </h4>
                </div>
              ))}
          </div>
        </div>
      ) : (
        <>
          <SectionHeading color={Colors.Bright}>Jelöltek</SectionHeading>
          <section id='landing-candidates'>
            {candidates.map(c => (
              <CandidateCard c={c} click={() => setCandidateModal(c)} key={c.id} />
            ))}
          </section>
          <h6>Kattints egy jelöltre, hogy elolvasd a teljes pályázatát!</h6>
        </>
      )}
      <section className='vote-info'>
        {window.innerWidth > 800 ? (
          <VIB id='vote-info-banner' />
        ) : (
          <VIBmobile id='vote-info-banner-mobile' />
        )}
        <div className='voteinfo-content'>
          <SectionHeading color={Colors.Dark}>A szavazás menete</SectionHeading>
          <div className='voteinfo-diagram'>
            <span className='voteinfo-diagram-line' />
            <div className='col'>
              <span />
              <h5>Személyes Adatok</h5>
              <p>
                Habár a szavazás teljesen anonim, néhány adatot mégis be kell gyűjtenünk, a csalások
                kiszűrése érdekében. Ne aggódj, az <b>OM azonosítón</b> kívül semmi másra nem
                vagyunk kíváncsiak. Arról, hogy ezeket az adatokat hogyan kezeljük, bővebben az{' '}
                <Link to='/privacy-policy'>Adatkezelési tájékoztatóban</Link> olvashatsz.
              </p>
            </div>
            <div className='col'>
              <span />
              <h5>Szavazat</h5>
              <p>
                Itt kell leadnod a szavazatodat. Jól gondold meg, kire szavazol, mert későbbi
                módosításra nincs lehetőség! Szavazás előtt érdemes végignézni minden jelölt
                pályázatát, hogy reálisan tudj dönteni.
              </p>
            </div>
            <div className='col'>
              <span />
              <h5>Kész!</h5>
              <p>
                Ha mindennel készen vagy, nincs más hátra, mint hogy megnyomd a gombot, és szurkolj
                a jelöltednek!
              </p>
            </div>
          </div>
        </div>
      </section>
      <footer>
        <div className='col'>
          <h2>Készült a Nyíregyházi Kölcsey Ferenc Gimnázium megbízásából.</h2>
          <h3>Készítette: Jelszo Co.</h3>
        </div>
        <div className='col'>
          <a href='mailto:support@jelszo.co'>Visszajelzés</a>
          <Link to='/privacy-policy'>Adatkezelési tájékoztató</Link>
          <Link to='/faq'>Gyakran Ismételt Kérdések</Link>
          <a href='https://kolcseygimnazium.hu'>kolcseygimnazium.hu</a>
        </div>
      </footer>
    </div>
  );
};

const mapStateToProps = (state: State) => ({
  pageStatus: state.pageStatus,
  candidates: state.candidates,
  candidateModal: state.candidateModal,
  timings: state.timings,
  user: state.user,
  winners: state.winnerCandidates,
  results: state.voteCount,
});

export default connect(mapStateToProps, { setCandidateModal })(withRouter(Landing));
