import ReactMarkdown from 'react-markdown';
import axios from 'axios';
import { useEffect, useState } from 'react';
import 'scss/MarkdownRenderer.scss';
import { chevronBackCircle } from 'ionicons/icons/index';
import IonIcon from 'pages/components/IonIcon';
import { RouteComponentProps, withRouter } from 'react-router-dom';

interface MarkdownProps extends RouteComponentProps {
  history: any;
  markdown: any;
  className?: string;
}

const MarkdownRenderer = ({ history, markdown, className }: MarkdownProps) => {
  const [md, setMd] = useState('');
  useEffect(() => {
    axios.get(markdown).then(res => setMd(res.data));
  }, []);
  return (
    <div id='markdown-renderer' className={className}>
      <div className='content'>
        <ReactMarkdown source={md} />
        <IonIcon
          icon={chevronBackCircle}
          className='chevron-icon'
          onClick={() => history.goBack()}
        />
      </div>
    </div>
  );
};
export default withRouter(MarkdownRenderer);
