import { useEffect, useState } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { PageStatus, State } from 'types';
import preloaderDark from 'assets/preloader_black.gif';
import CSS from 'csstype';

import 'scss/components/Preloader.scss';

const mapStateToProps = (state: State) => ({
  status: state.pageStatus,
});

const connector = connect(mapStateToProps, {});

const Preloader = ({ status }: ConnectedProps<typeof connector>) => {
  const [style, setStyle] = useState<CSS.Properties>({ opacity: 1, display: 'block' });

  useEffect(() => {
    if (status !== PageStatus.Loading) {
      setTimeout(() => {
        setStyle({ ...style, opacity: 0 });
        setTimeout(() => {
          setStyle({ ...style, display: 'none' });
        }, 400);
      }, 500);
    }
  }, [status]);
  return (
    <div id={'preloader'} style={style}>
      <img src={preloaderDark} alt={'Loading...'} />
    </div>
  );
};

export default connector(Preloader);
