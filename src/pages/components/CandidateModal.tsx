import { Colors, GeneralCTA, SectionHeading } from '@jelszo-co/dok-components';
import { chevronUp, close, cloudDownloadOutline, personOutline } from 'ionicons/icons';
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Candidate, PageStatus, State } from 'types';
import IonIcon from './IonIcon';
import { Document, Page } from 'react-pdf/dist/esm/entry.webpack';
import times from 'lodash/times';
import isEmpty from 'lodash/isEmpty';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { delCandidateModal } from 'actions/candidateModal';
import { updateVoteData } from 'actions/vote';
import isMobile from 'utils/isMobile';
import classNames from 'classnames';
import axios from 'axios';
import preloader from 'assets/preloader_bright.gif';
import 'scss/components/CandidateModal.scss';

interface CandidateModalProps extends RouteComponentProps {
  c: Candidate;
  delCandidateModal: Function;
  updateVoteData: Function;
  status: PageStatus;
}

const CandidateModal = ({
  c,
  status,
  delCandidateModal,
  updateVoteData,
  history,
}: CandidateModalProps) => {
  const [numPages, setNumPages] = useState(0);
  const [file, setFile] = useState('');
  useEffect(() => window.scroll({ top: 0, left: 0, behavior: 'smooth' }), []);
  useEffect(() => {
    axios
      .get(`${process.env.REACT_APP_SRV_ADDR}/candidate/essay/${c.id}`)
      .then(r => setFile(r.data.essay));
  }, []);

  const handleCTAClick = () => {
    updateVoteData({ chosenCandidate: c.id });
    history.push('/vote');
    delCandidateModal();
  };

  const handlePDFLoad = (numPages: number) => {
    setNumPages(numPages);
  };

  const renderEssay = () => {
    if (isEmpty(file)) return <img src={preloader} alt={'Töltés...'} />;
    if (isMobile())
      return (
        <>
          <IonIcon icon={cloudDownloadOutline} />
          <a
            href={`data:application/pdf;base64,${file}`}
            download={`program-${c.name.map(n => n.toLowerCase().replace(' ', '_')).join('-')}`}>
            Letöltés
          </a>
        </>
      );

    return (
      <Document
        file={`data:application/pdf;base64,${file}`}
        onLoadSuccess={({ numPages: numPages }) => handlePDFLoad(numPages)}>
        {times(numPages, i => (
          <Page pageNumber={i + 1} key={i} scale={1.5} renderTextLayer={false} />
        ))}
      </Document>
    );
  };

  return (
    <div id='candidate-modal'>
      <IonIcon
        icon={close}
        className='candidate-modal-close'
        onClick={() => {
          delCandidateModal();
        }}
      />
      <div className='candidate-modal-card'>
        {c.picture ? (
          <img src={`data:image/png;base64,${c.picture}`} alt='' />
        ) : (
          <IonIcon icon={personOutline} className='candidate-modal-img-placeholder' />
        )}
        <div className='candidate-modal-card-col'>
          {c.name.map(name => (
            <h3 className='candidate-name' key={name}>
              {name}
            </h3>
          ))}
          {status === PageStatus.Vote && (
            <GeneralCTA
              color={Colors.Bright}
              bgColor={Colors.Gradient}
              callback={() => handleCTAClick()}>
              {c.name.length > 1 ? 'rájuk' : 'rá'} szavazok!
            </GeneralCTA>
          )}
        </div>
      </div>
      <SectionHeading color={Colors.Dark}>Bemutatkozás</SectionHeading>
      {c.description && <p className='candidate-modal-description'>{c.description}</p>}
      <SectionHeading color={Colors.Dark}>Program</SectionHeading>
      <div
        className={classNames('candidate-modal-essay', {
          'candidate-modal-essay-mobile': isMobile(),
        })}>
        {renderEssay()}
      </div>
      {status === PageStatus.Vote && (
        <GeneralCTA
          color={Colors.Bright}
          bgColor={Colors.Gradient}
          callback={() => handleCTAClick()}
          className='candidate-modal-cta-bottom'>
          {c.name.length > 1 ? 'rájuk' : 'rá'} szavazok!
        </GeneralCTA>
      )}
      <div
        className='scrolltop'
        onClick={() => window.scroll({ top: 0, left: 0, behavior: 'smooth' })}>
        <IonIcon icon={chevronUp} className=' scrolltop-icon' />
        <p>Az oldal tetejére</p>
      </div>
    </div>
  );
};

const mapStateToProps = (state: State) => ({
  c: state.candidateModal,
  status: state.pageStatus,
});

export default connect(mapStateToProps, { delCandidateModal, updateVoteData })(
  withRouter(CandidateModal),
);
