import React, { ReactElement } from 'react';

import '../scss/Parking.scss';

const Parking = (): ReactElement => (
  <div id='parking'>
    <div className='content'>
      <h1>Indulás hamarosan...</h1>
    </div>
  </div>
);

export default Parking;
