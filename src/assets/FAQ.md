# Gyakran Ismételt Kérdések

## Mi ez az oldal?

Az oldalon keresztül a diákok szavazhatnak az általuk kedvelt diákönkormányzati elnökre/elnökökre.

## Hogyan tudok szavazni, mire lesz szükségem?

Bármelyik szavazás gombbal tudsz szavazni, ezután szükség lesz az oktatási azonosítód megadására (11 számjegyű, 7-essel kezdődő számsor a diákigazolványodon), majd végül kiválaszthatod a jelöltek közül a neked legszimpatikusabbat.

## Mennyire anonim a szavazás?

Akárcsak a jelenléti papír alapú szavazásnál, senki nem fogja tudni, hogy Te kire szavaztál.

## Miért szükséges megadni az OM azonosítómat, lehet-e anélkül szavazni?

Az oktatási azonosító segítségével tudjuk kiszűrni az esetleges csalásokat, valamint azt, hogy csak a rá jogosult személyek (a Nyíregyházi Kölcsey Ferenc Gimnázium tanulói) tudjanak szavazni. Nem, sajnos OM azonosító hiányában nem fogod tudni leadni a szavazatod.

## Biztonságos-e megadnom az oktatási azonosítómat?

A felhasználótól származó adatok titkosítva érkeznek meg a szerverhez, ahol biztonságosan tároljuk őket, így a megadott adat titkosítás után nem fejthető vissza. Erről bővebben az [adatkezelési tájékoztatóban](/privacy-policy) olvashatsz.

## Hol tudom majd megnézni a győzteseket?

Amint az iskola úgy határoz és lezárja a szavazást, az oldalon a továbbiakban nem lesz lehetőség szavazni és a főoldalon megjelenik majd a győztes.

## Véletlenül rossz emberre szavaztam, van-e lehetőség a változtatásra?

Nem, sajnos utólagos módosításra nincs lehetőség.

## Kérdésem van, illetve segítséget szeretnék kérni, hol tudom ezt megtenni?

A visszajelzéseket, kérdéseket, észrevételeket a [support@jelszo.co](mailto:support@jelszo.co) e-mail címre írhatod.
