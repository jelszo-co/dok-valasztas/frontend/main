# Adatkezelési Tájékoztató

> Kiadva: 2021. 01.20.
>
> Érvényesség: 2022. 01.01.

A felület használatával a felhasználó automatikusan elfogadja az adatkezelési tájékoztatóban foglaltakat. Amennyiben a felhasználó az adatkezelési tájékoztatóban foglaltakkal nem ért egyet, a felület használata számára tilos.

## I. Felelősségvállalás

Mi, a készítők semmilyen felelősséget nem vállalunk a Nyíregyházi Kölcsey Ferenc Gimnázium (továbbiakban "**iskola**") éves Diákönkormányzati Elnöki választását (továbbiakban "**szavazás, választás**") lebonyolító online felület (továbbiakban "**oldal**") helytelen használatából eredő adatvesztésért.

## II. A szavazás lebonyolításához szükséges felhasználói személyes adatok

### 1. A felhasznált felhasználói személyes adatok teljes jegyzéke

A szavazás lebonyolításához a következő felhasználói személyes adatokat (alrészben továbbiakban "**adatok**") gyűjtjük és használjuk fel:

- Oktatási Azonosító szám,
- Választott diákönkormányzati elnökjelölt.

### 2. Az adatok gyűjtésének és felhasználásának indoka

Az adatok gyűjtésének indoka a felhasználók egyedi azonosítása, a felhasználók deanonimizálás nélküli szűrése, a szavazás szabályos lefolyásának biztosítása és a csalások kiszűrése.

- **A felhasználók egyedi azonosítása** Az adatok ezen felhasználása lehetővé teszi azt, hogy egy felhasználót egy másik felhasználótól minden kétséget kizárva azonosítsunk, ezáltal az ún. _versenyhelyzet_ lehetőségét kizárjuk.

- **Felhasználók deanonimizálás nélküli szűrése** Az adatok ezen felhasználása lehetővé teszi azt, hogy csak az iskolával tanulói jogviszonyban állók (továbbiakban "**tanulók, diákok**") legyenek jogosultak a szavazásban részt venni.

- **A szavazás szabályos lefolyásának biztosítása, a csalások kiszűrése** Az adatok ezen felhasználása lehetővé teszi azt, hogy egy diák maximum egy alkalommal adhasson le szavazatot, és a konzekvens próbálkozásai érvénytelenné váljanak.

### 3. Az adatok anonimitása

Mivel a szavazás anonim, így a diákok teljes neve helyett az azonosításuk az oktatási azonosító számmal történik. Ily módon egy diák saját adatai ismeretében adhat le szavazatot, viszont mi, vagy más diákok sem a nyers adattal, sem az adat tárolt, titkosított (lásd: **II./6**.) formájával nem tudjuk beazonosítani a szavazót.

### 4. Az adatok forrása

Az adatok felhasználási módjából következően az iskola a felhasználásunkra bocsájtott egy adattáblát, amelyben oktatási azonosító számok szerepelnek. Az oldal adatbázisa erre a struktúrára épül.

### 5. Az adatok gyűjtése

Az adatok gyűjtése teljesen önkéntes alapú, az oldal semmilyen, a szavazás lebonyolításához szükséges személyes adatot nem rögzít mindaddig, ameddig a felhasználó ezeket meg nem adja. Az adatok gyűjtése az oldalon egy űrlapon keresztül történik, ahol a felhasználó számára érthető módon van feltüntetve, hogy az oldal éppen milyen adatot gyűjt.

### 6. Az adatok rövidtávú tárolása

Az adatok a Linode, LLC privát szerverein (továbbiakban "**szerver**") vannak tárolva, ahol különböző adatfeldolgozó és adattároló rendszereken (lásd: oldal forráskódja) fut át. Az adatok biztonságáért a szerver vállal felelősséget.

Az adatok a szerverre SSL-TLS típusú titkosítással érkeznek meg, ahol a szerver egy ún. _hasítófüggvénnyel_ titkosítja, majd tárolja azokat. Mivel a függvény egyirányú, ezáltal egy lehetséges adatlopás esetén az ellopott adat nem fejthető vissza az eredeti adatpárosra.

### 7. Az adatok hosszútávú őrzése

Az adatok a tanév végével (2021.06.15.) kezdődően hosszútávúan tárolandó adatoknak minősülnek. Az adatokkal való hosszútávú rendelkezés az iskola joga és felelőssége. A készítők semmilyen felelősséget nem vállalnak a hosszútávú adatokért.

## III. Statisztikai és kiberbiztonsági adatok

### 1. Az oldal látogatottsági statisztikáival kapcsolatosan gyűjtött adatok teljes jegyzéke

Az oldal látogatottsági statisztikáival kapcsolatos adatokból (alrészben továbbiakban "**adatok**") a következőket gyűjtjük és használjuk fel:

- Az oldal felhasználóinak összesített száma,
- Az oldal felhasználóinak Internet Protokol címe\*,
- Az oldal felhasználói által látogatott aloldalak neve,
- Az oldalra érkező felhasználók eredete,
- Az oldal felhasználóinak az oldalon töltött ideje,
- Az oldal felhasználói által felhasznált böngészőprogram nyelve.

\* lásd: **A1.**

### 2. Az adatok gyűjtésének és felhasználásának indoka

Az adatok gyűjtésének indoka az oldal látogatottságával kapcsolatos statisztikák kimutatása iskolai alkalmazottak számára és az oldal kibertámadásokkal szembeni védelme.

- **Az oldal látogatottságával kapcsolatos statisztikák kimutatása iskolai alkalmazottak számára** Az adatok ezen felhasználása lehetővé teszi azt, hogy a szavazás közben és után az iskola alkalmazottjainak lehetősége legyen betekintést nyerni a fent említett statisztikai adatok által szolgáltatott kimutatásokba.
- **Az oldal kibertámadásokkal szembeni védelme** Az adatok ezen felhasználása lehetővé teszi azt, hogy az oldal ellen indított ún. _Brute Force_ támadás (lásd: **A1.**) kivédhetőek legyenek.

### 4. Az adatok forrása

Az adatok megosztottan származhatnak saját adatkezelési rendszerekből, és harmadik féltől származó rendszerektől (lásd: **IV./3.**).

### 5. Az adatok tárolása

Az adatokat a Linode, LLC privát szerverein tároljuk, a szavazás adataival együtt.

### 6. Az adatok hosszútávú őrzése

Az adatok a tanév végével (2021.06.15.) kezdődően hosszútávúan tárolandó adatoknak minősülnek. Az adatokkal való hosszútávú rendelkezés az iskola joga és felelőssége. A készítők semmilyen felelősséget nem vállalnak a hosszútávú adatokért.

## IV. Harmadik féltől származó rendszerek adatvédelmi irányelvei

### 1. Linode, LLC

A Linode, LLC által nyújtott privát szerver az oldal működéséhez nélkülözhetetlen komponens.

A Linode, LLC adatvédelmi irányelvei [itt](https://www.linode.com/legal-privacy/) érhetőek el.

### 2. Cloudflare, Inc.

A Cloudflare, Inc. által nyújtott tartománynévrendszer-szerver nagyban elősegíti az ún. _DDoS_ (lásd: **A1.**) támadások elleni védekezést.

A Cloudflare, Inc. adatvédelmi irányelvei [itt](https://www.cloudflare.com/privacypolicy/) érhetőek el.

## A1. Az oldal ellen indítható kibertámadások elleni védelem

Egy weboldalnak számtalan kibertámadással szemben kell felkészültnek lennie. Ezek közül a legfontosabbak:

### 1. DDoS (Distributed Denial of Service)

Az ún. _DDoS_ támadások olyan számítógéphálózatról indított támadások, amelynek leggyakoribb célja egy weboldal működésképtelenné tétele. Ezen támadások kivédése érdekében az oldal A Cloudflare, Inc. által nyújtott szolgáltatásokat veszi igénybe.

### 2. Brute Force

Az ún. _Brute Force_ támadások olyan kibertámadások, amelynek leggyakoribb célja az oldalon található űrlapok feltörése egy olyan módszerrel, amely minden lehetséges kombinációt kipróbál, amíg el nem talál egyet. Ezen támadások kivédése érdekében a szerveren egy ún. _fail2ban_ technológiát használ, amely letilt minden olyan Internet Protokol címet, amelyről adott időn belül adott mennyiség fölötti kérés érkezik be. A tiltáshoz a technológia Internet Protokol címet használ, amely csak a tiltás életbe lépésével egyidőben kerül tárolásra.
