import { GET_WINNER_CANDIDATES } from 'actions/types';
import { API_candidate_winner__instance } from '../types/apiTypes';

const initialSate: API_candidate_winner__instance[] = [];

export default (
  state = initialSate,
  { type, payload }: { type: string; payload: any },
): API_candidate_winner__instance[] => {
  switch (type) {
    case GET_WINNER_CANDIDATES:
      return payload;
    default:
      return state;
  }
};
