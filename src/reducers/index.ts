import { combineReducers } from 'redux';
import pageStatus from './pageStatus';
import popups from './popup';
import candidates from './candidates';
import candidateModal from './candidateModal';
import winnerCandidates from './winnerCandidates';
import voteCount from './voteCount';
import vote from './vote';
import user from './user';
import timings from './timings';

const reducerList = {
  pageStatus,
  timings,
  popups,
  candidates,
  candidateModal,
  winnerCandidates,
  voteCount,
  vote,
  user,
};

export default combineReducers(reducerList);
