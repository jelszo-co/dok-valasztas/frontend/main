import { Timing } from 'types';
import { GET_TIMINGS } from 'actions/types';

const initialState: Timing[] = []

export default (state = initialState, {type, payload}: {type: string, payload: any}): Timing[] => {
  switch(type) {
    case GET_TIMINGS:
      return payload;
      default:
      return state;
  }
}
