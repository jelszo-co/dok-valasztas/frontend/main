import { GET_VOTE_COUNT } from 'actions/types';
import { API_candidate_results__instance } from '../types/apiTypes';

const initialSate: API_candidate_results__instance[] = [];

export default (state = initialSate, { type, payload }: { type: string; payload: any }): any => {
  switch (type) {
    case GET_VOTE_COUNT:
      return payload;
    default:
      return state;
  }
};
