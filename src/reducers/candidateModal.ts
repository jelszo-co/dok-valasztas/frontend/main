import { DEL_CANDIDATE_MODAL, SET_CANDIDATE_MODAL } from 'actions/types';
import { Candidate } from 'types';

const initialSate: Candidate | null = null;

export default (
  state = initialSate,
  { type, payload }: { type: string; payload: any },
): Candidate | null => {
  switch (type) {
    case SET_CANDIDATE_MODAL:
      return payload;
    case DEL_CANDIDATE_MODAL:
      return null;
    default:
      return state;
  }
};
