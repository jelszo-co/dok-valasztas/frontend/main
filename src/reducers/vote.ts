import { UPDATE_VOTE_DATA, UPDATE_VOTE_PROGRESS, UPDATE_VOTE_STAGE } from 'actions/types';
import { VoteData } from 'types/stateTypes';

const initialSate: VoteData = {
  om: '',
  chosenCandidate: undefined,
  stage: 0,
  progress: 0,
};

export default (
  state = initialSate,
  { type, payload }: { type: string; payload: any },
): VoteData => {
  switch (type) {
    case UPDATE_VOTE_DATA:
      return { ...state, ...payload };

    case UPDATE_VOTE_STAGE: {
      return { ...state, stage: payload };
    }
    case UPDATE_VOTE_PROGRESS: {
      return { ...state, progress: payload };
    }
    default:
      return state;
  }
};
