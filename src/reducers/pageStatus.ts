import { GET_PAGE_STATUS } from 'actions/types';
import { PageStatus } from 'types';

const initialState: PageStatus = PageStatus.Loading;

export default (
  state = initialState,
  { type, payload }: { type: string; payload: any },
): PageStatus => {
  switch (type) {
    case GET_PAGE_STATUS:
      return payload;
    default:
      return state;
  }
};
