import { SET_USER_BANNED } from 'actions/types';
import { UserData } from 'types/stateTypes';

const initialSate: UserData = {
  banned: false,
};

export default (
  state = initialSate,
  { type, payload }: { type: string; payload: any },
): UserData => {
  switch (type) {
    case SET_USER_BANNED:
      return { banned: payload.banned, bannedTill: payload.bannedTill };
    default:
      return state;
  }
};
