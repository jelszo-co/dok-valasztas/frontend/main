import { GET_CANDIDATES } from 'actions/types';
import { Candidate } from 'types';

const initialSate: Candidate[] = [];

export default (
  state = initialSate,
  { type, payload }: { type: string; payload: any },
): Candidate[] => {
  switch (type) {
    case GET_CANDIDATES:
      return payload;
    default:
      return state;
  }
};
