import { UserData, VoteData } from 'types/stateTypes';
import { API_candidate_results__instance, API_candidate_winner__instance } from './types/apiTypes';

export interface State {
  pageStatus: PageStatus;
  timings: Timing[];
  popups: Popup[];
  candidates: Candidate[];
  candidateModal: Candidate;
  vote: VoteData;
  user: UserData;
  winnerCandidates: API_candidate_winner__instance[];
  voteCount: API_candidate_results__instance[];
}

export enum PageStatus {
  Loading = 'LOADING',
  Closed = 'CLOSED',
  Open = 'OPEN',
  Vote = 'VOTE',
  VoteExpired = 'VOTE_EXPIRED',
  Result = 'RESULT',
}

export interface Timing {
  start: string;
  status: PageStatus;
  until: string;
}

export interface Popup {
  id: string;
  msg: string;
  type: PopupType;
  expireIn?: number;
}

export enum PopupType {
  Info,
  Alert,
  Error,
}

export interface Candidate {
  id: number;
  name: string[];
  description?: string;
  picture?: string;
}
